FROM alpine:latest

RUN apk add --update nginx

EXPOSE 80

CMD [ "nginx", "-g", "daemon off;" ]